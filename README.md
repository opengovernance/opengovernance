# OpenGovernance

Trying to fill the role in the analogy:

```
Open source copyrighting, liability, warranty specification <--> GNU License
Open source project governance                              <--> ___________
```

# Contact Us / Get Involved

opengovernance@janie.page

#opengovernance:matrix.org

# Sales Pitches (Getting Others Involved)

- Wikipedia meets Github.
- Abolish private property (private ownership of OSS).
- Benevolent dictators for life are bad, actually.

# Details...

Forthcoming. We're going to start with a CSS LSP called `Classy` developed with
this governance model.
